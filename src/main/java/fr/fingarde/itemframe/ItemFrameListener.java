package fr.fingarde.itemframe;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class ItemFrameListener implements Listener {
    @EventHandler(priority = EventPriority.HIGH)
    public void onClick(PlayerInteractEntityEvent event) {
        if(event.isCancelled()) return;

        if(event.getHand() != EquipmentSlot.HAND) return;
        if(! (event.getRightClicked() instanceof ItemFrame)) return;

        ItemFrame entity = (ItemFrame) event.getRightClicked();

        Block facing = entity.getLocation().getBlock().getRelative(entity.getAttachedFace());

        if(facing.getType().isInteractable()) {
            PlayerInteractEvent interactBlock = new PlayerInteractEvent(event.getPlayer(), Action.RIGHT_CLICK_BLOCK, event.getPlayer().getInventory().getItemInMainHand(), facing, entity.getAttachedFace());
            Main.instance.getServer().getPluginManager().callEvent(interactBlock);

            if (interactBlock.useInteractedBlock() == Event.Result.DENY) {
                entity.setRotation(entity.getRotation().rotateCounterClockwise());
                return;
            }
        }

        if(event.getPlayer().isSneaking() && entity.getItem().getType() != Material.AIR) {
            entity.setRotation(entity.getRotation().rotateCounterClockwise());

            entity.setVisible(!entity.isVisible());
            return;
        }

        if(entity.isVisible()) return;

        entity.setRotation(entity.getRotation().rotateCounterClockwise());

        if(! (facing.getState() instanceof InventoryHolder)) return;

        if (facing.getState() instanceof Chest) {
            if (getRelativeChestFace(facing) != null &&
                facing.getRelative(getRelativeChestFace(facing)).getRelative(0, 1, 0).getType().isOccluding()
            )
                return;
            if (facing.getRelative(0, 1, 0).getType().isOccluding()) return;

        }

        Inventory inv = ((InventoryHolder) facing.getState()).getInventory();
        event.getPlayer().openInventory(inv);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if(! Bukkit.getVersion().contains("1.16")) return;

        if(!(event.getInventory().getHolder() instanceof DoubleChest)) return;
        DoubleChest b = (DoubleChest) event.getInventory().getHolder();

        NMS.playChestCloseAnimation(((Chest) b.getLeftSide()).getBlock());
        NMS.playChestCloseAnimation(((Chest) b.getRightSide()).getBlock());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDamage(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) return;

        if(! (event.getEntityType().name().contains("ITEM_FRAME"))) return;

        ItemFrame entity = (ItemFrame) event.getEntity();

        if(entity.isVisible()) return;

        event.setCancelled(true);
    }

    public static BlockFace getRelativeChestFace(Block block) {
        org.bukkit.block.data.type.Chest chest = (org.bukkit.block.data.type.Chest) block.getBlockData();
        BlockFace face = (((org.bukkit.block.data.type.Chest) block.getBlockData()).getFacing());
        BlockFace relativeFace = null;
        if (chest.getType() == org.bukkit.block.data.type.Chest.Type.LEFT) {
            if (face == BlockFace.NORTH) {
                relativeFace = BlockFace.EAST;
            } else if (face == BlockFace.SOUTH) {
                relativeFace = BlockFace.WEST;
            } else if (face == BlockFace.WEST) {
                relativeFace = BlockFace.NORTH;
            } else if (face == BlockFace.EAST) {
                relativeFace = BlockFace.SOUTH;
            }
        } else if (chest.getType() == org.bukkit.block.data.type.Chest.Type.RIGHT) {
            if (face == BlockFace.NORTH) {
                relativeFace = BlockFace.WEST;
            } else if (face == BlockFace.SOUTH) {
                relativeFace = BlockFace.EAST;
            } else if (face == BlockFace.WEST) {
                relativeFace = BlockFace.SOUTH;
            } else if (face == BlockFace.EAST) {
                relativeFace = BlockFace.NORTH;
            }
        }
        return relativeFace;
    }
}
