package fr.fingarde.itemframe;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static JavaPlugin instance;
    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;

        getServer().getPluginManager().registerEvents(new ItemFrameListener(), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
