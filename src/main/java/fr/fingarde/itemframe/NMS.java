package fr.fingarde.itemframe;

import org.bukkit.Server;
import org.bukkit.block.Block;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NMS {
    public static String getVersion(Server server) {
        final String packageName = server.getClass().getPackage().getName();

        return packageName.substring(packageName.lastIndexOf('.') + 1);
    }

    public static void playChestCloseAnimation(Block chest) {
       try {
            String version = getVersion(Main.instance.getServer());

            String nms = "net.minecraft.server." + version + ".";
            String obc = "org.bukkit.craftbukkit." + version + ".";

            Class classBlockPosition = Class.forName(nms + "BlockPosition");
            Class classCraftBlock = Class.forName(obc + "block.CraftBlock");
            Class classCraftWorld = Class.forName(obc + "CraftWorld");
            Class classWorldServer = Class.forName(nms + "WorldServer");
            Class classBlock = Class.forName(nms + "Block");

            Object craftWorld = classCraftWorld.cast(chest.getLocation().getWorld());
            Object craftBlock = classCraftBlock.cast(chest);

            Constructor constructorBlockPosition = classBlockPosition.getConstructor(int.class, int.class, int.class);

            Method methodGetHandleBlock = classCraftBlock.getDeclaredMethod("getNMSBlock");
            Method methodGetHandleWorld = classCraftWorld.getDeclaredMethod("getHandle");
            Method methodPlayBlockAction = classWorldServer.getDeclaredMethod("playBlockAction", classBlockPosition, classBlock, int.class, int.class);

            methodGetHandleBlock.setAccessible(true);
            Object nmsBlock = methodGetHandleBlock.invoke(craftBlock);
            Object handleWorld = methodGetHandleWorld.invoke(craftWorld);
            Object blockPosition = constructorBlockPosition.newInstance(chest.getX(), chest.getY(), chest.getZ());

            methodPlayBlockAction.invoke(handleWorld, blockPosition, nmsBlock, 1, 0);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
